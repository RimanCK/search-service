#!/bin/bash

if [[ ${ENVIRONMENT} = 'LOCAL' ]]; then
    uvicorn asgi:app --host 0.0.0.0 --port 5000 --reload
    exec echo 'api'
elif [[ ${ENVIRONMENT} = 'WORKER_MODE' ]]; then
    dramatiq search_service.tasks --threads 1
    exec echo 'worker'
fi
