from search_service.app.exceptions import AppBaseException


MESSAGE_PREFIX: str = "SEARCH "


class SearchBaseException(AppBaseException):
    def __init__(self, message: str):
        super().__init__(f"{MESSAGE_PREFIX}:{message}")
