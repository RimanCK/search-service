from typing import AsyncContextManager

from dependency_injector.wiring import Provide, Container
from fastapi import Depends


class BaseQuery:
    def __init__(self, manticore_session_context: AsyncContextManager):
        self.manticore_session_context: AsyncContextManager = manticore_session_context

    def search(self, search_query: str):
        raise NotImplemented
