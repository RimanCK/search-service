from typing import AsyncContextManager

from search_service.services import CategoryRepository
from search_service.services.search.queries.base_query import BaseQuery


class SearchByCategory(BaseQuery):
    def search(self, search_query: str):
        async with self.manticore_session_context as session:
            category_repository: CategoryRepository = CategoryRepository(session)

            # category_repository.get_by_name()
