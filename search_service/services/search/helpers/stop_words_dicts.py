class StopWordCache:
    def __init__(self, file_with_stop_words: str):
        self.stop_words: list[str] = self.load(file_with_stop_words)

    @staticmethod
    def load(dict_stop_words_path: str):
        with open(dict_stop_words_path, "r") as file:
            return file.read().split("\n")

    def get_stop_words(self) -> list[str]:
        return self.stop_words
