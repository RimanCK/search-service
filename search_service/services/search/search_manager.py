class SearchManager:
    def __init__(self, queries: list = None):
        if queries is None:
            queries = []
        self.queries = queries

    async def search(self, query_string: str):
        return [query.search(query_string) for query in self.queries]
