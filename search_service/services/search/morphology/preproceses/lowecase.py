from search_service.services.search.morphology.preproceses.interfaces import QueryPreprocessAbstract


class PreprocessLowerCase(QueryPreprocessAbstract):
    def process(self, search_query: str) -> str:
        return search_query.lower()
