from abc import ABC, abstractmethod


class QueryPreprocessAbstract(ABC):
    @abstractmethod
    def process(self, search_query: str) -> str:
        pass
