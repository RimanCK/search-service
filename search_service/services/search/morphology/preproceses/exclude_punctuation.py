import string


from search_service.services.search.morphology.preproceses.interfaces import QueryPreprocessAbstract


class PreprocessExcludePunctuation(QueryPreprocessAbstract):
    def __init__(self):
        self.table_punctuation = str.maketrans(dict.fromkeys(string.punctuation))

    def process(self, search_query: str) -> str:
        search_query_without_punctuation: str = search_query.translate(self.table_punctuation)

        return search_query_without_punctuation.strip()
