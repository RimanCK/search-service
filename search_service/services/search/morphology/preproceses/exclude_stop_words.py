import string


from search_service.services.search.morphology.preproceses.interfaces import QueryPreprocessAbstract


class PreprocessExcludeStopWords(QueryPreprocessAbstract):
    def __init__(self, stop_words: set[str]):
        self._stop_words: set[str] = stop_words

    def process(self, search_query: str) -> str:

        search_query_without_stop_words = [word for word in search_query.split(" ") if word not in self._stop_words]

        return " ".join(search_query_without_stop_words)
