from abc import ABC, abstractmethod

from search_service.services.search.morphology.preproceses.interfaces import QueryPreprocessAbstract


class NLPHandlerAbstract(ABC):
    def __init__(self, preprocesses: list[QueryPreprocessAbstract]):
        self.preprocess_chain: list[QueryPreprocessAbstract] = preprocesses

    @abstractmethod
    def handle(self, search_query: str) -> list[str]:
        pass
