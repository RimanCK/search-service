from search_service.services.search.morphology.interfaces import NLPHandlerAbstract
from search_service.services.search.morphology.preproceses.interfaces import QueryPreprocessAbstract


class NLPHandler(NLPHandlerAbstract):
    def handle(self, search_query: str) -> list[str]:

        preprocess: QueryPreprocessAbstract
        for preprocess in self.preprocess_chain:
            search_query: str = preprocess.process(search_query)  # type: ignore

        return search_query.split(" ")
