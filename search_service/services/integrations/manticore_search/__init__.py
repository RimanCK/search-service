__all__ = [
    "SearchService",
]

from search_service.services.integrations.manticore_search.manticore import SearchService
