from abc import ABC, abstractmethod


class ManticoreSearchAbstract(ABC):
    @abstractmethod
    async def _execute_query(self, query: str) -> None | str:
        pass

    @abstractmethod
    async def replace(self, index_name: str, values: dict) -> None:
        pass

    @abstractmethod
    async def insert(self, index_name: str, values: dict) -> None:
        pass

    @abstractmethod
    async def delete_by_equality(self, index_name: str, field: str, value: int | str) -> None:
        pass
