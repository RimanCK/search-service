import re

from sqlalchemy.engine import Result
from sqlalchemy.exc import DBAPIError
from sqlalchemy.ext.asyncio import AsyncSession

from search_service.services.integrations.manticore_search import sql_collection
from search_service.services.integrations.manticore_search.exceptions import (
    ManticoreSearchDBAPIError,
)
from search_service.services.integrations.manticore_search.interfaces import (
    ManticoreSearchAbstract,
)


class SearchService(ManticoreSearchAbstract):
    def __init__(self, manticore_session: AsyncSession):
        self.session_manticore: AsyncSession = manticore_session

    def _serialize_data(self, columns: dict) -> tuple[str, str]:
        result_string: list[str] = []

        columns_without_none: dict = self._remove_none(columns)

        key: str
        value: list | str | int
        for value in columns_without_none.values():
            if isinstance(value, list):
                list_str = ", ".join(map(str, value))
                result_string.append(f"({list_str})")
            elif isinstance(value, str):
                result_string.append(f"'{value}'")
            elif isinstance(value, bool):
                result_string.append(str(int(value)))
            else:
                result_string.append(str(value))

        values: str = ", ".join(result_string)
        fields: str = ", ".join(columns_without_none.keys())

        return fields, values

    #
    # def _serialize_for_match(self, columns: dict[str, str]):
    #     """
    #     raise: ValueError
    #     """
    #     values_string: list[str] = []
    #     fields_string: list[str] = []
    #
    #     columns_without_none: dict = self._remove_none(columns)
    #
    #     key: str
    #     value: list | str | int
    #     # for value in columns_without_none.values():
    #     #     if not isinstance(value, str):
    #     #         raise ValueError("For match search, only strings type value")
    #
    #     fields_string: str = f"({','.join(columns_without_none.keys())})"
    #
    #     fields: str = ", ".join(columns_without_none.keys())
    #     values = ", ".join(values_string)
    #
    #     return fields, f"({values})"

    def _deserialize_data(self, columns: dict):
        key: str
        value: str
        for key, value in columns.items():
            if isinstance(value, str):
                if re.findall("^\d+,", value):
                    columns[key] = list(map(int, value.split(",")))
                else:
                    columns[key] = value
        return columns

    def _remove_none(self, value: dict) -> dict:
        return {k: v for k, v in value.items() if v is not None}

    async def _execute_query(self, query: str) -> None | Result:
        """
        Executes a request to manticore search.

        :raise ManticoreSearchDBAPIError
        """

        try:
            return await self.session_manticore.execute(query)
        except DBAPIError as exc:
            raise ManticoreSearchDBAPIError(str(exc))

    async def replace(self, index_name: str, columns: dict) -> None:
        """
        :raise ManticoreSearchDBAPIError
        """
        fields, values = self._serialize_data(columns)

        query: str = sql_collection.REPLACE.format(
            index_name=index_name,
            field_list=fields,
            value_list=values,
        )

        await self._execute_query(query)

    async def insert(self, index_name: str, columns: dict) -> None:
        """
        :raise ManticoreSearchDBAPIError
        """
        fields, values = self._serialize_data(columns)

        query: str = sql_collection.INSERT.format(
            index_name=index_name,
            field_list=fields,
            value_list=values,
        )

        await self._execute_query(query)

    async def delete_by_equality(self, index_name: str, field: str, value: int | str) -> None:
        """
        :raise ManticoreSearchDBAPIError
        """
        query: str = sql_collection.DELETE_BY_FIELD.format(index_name=index_name, field=field, value=value)

        await self._execute_query(query)

    async def select_count_rows(self, index_name: str, field: str, value: int | str):
        query: str = sql_collection.CHECK_EXIST_BY_FIELD.format(index_name=index_name, field=field, value=value)

        res = await self._execute_query(query)
        return res.scalars().one()

    async def select_row_by_field(self, index_name: str, field: str, value: int | str) -> dict | None:
        query: str = sql_collection.SELECT_ROW_BY_ID.format(index_name=index_name, field=field, value=value)

        res = await self._execute_query(query)
        row: list[tuple] | None = res.one_or_none()
        return self._deserialize_data(dict(zip(res.keys(), row))) if row else None

    async def select_rows_by_field(self, index_name: str, field: str, value: int | str) -> list[dict]:
        query: str = sql_collection.SELECT_ROW_BY_ID.format(index_name=index_name, field=field, value=value)

        res = await self._execute_query(query)
        rows: list[tuple] | None = res.all()

        data: list[dict] = []

        row: tuple
        for row in rows:
            data.append(self._deserialize_data(dict(zip(res.keys(), row))))

        return data

    async def suggest_with_limit(self, index_name: str, word: str, limit: int) -> list[dict]:
        query: str = sql_collection.SUGGESTION_WITH_LIMIT.format(index_name=index_name, word=word, limit_value=limit)

        res = await self._execute_query(query)
        rows: list[tuple] | None = res.all()

        data: list[dict] = []

        row: tuple
        for row in rows:
            data.append(self._deserialize_data(dict(zip(res.keys(), row))))

        return data

    # async def match_by_with_or(self, index_name: str, columns: dict[str, str]):
    #     """
    #     :raise ManticoreSearchDBAPIError
    #     """
    #     fields, values = self._serialize_data(columns)
    #
    #     query: str = sql_collection.MATCH.format(
    #         index_name=index_name,
    #         field_list=fields,
    #         value_list=values,
    #     )
    #
    #     await self._execute_query(query)
