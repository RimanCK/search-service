from search_service.app.exceptions import AppBaseException


class ManticoreSearchBaseException(AppBaseException):
    def __init__(self, message: str):
        super().__init__(message)


class ManticoreSearchDBAPIError(ManticoreSearchBaseException):
    pass
