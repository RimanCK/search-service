from pydantic import Field, validator, BaseModel


class OfferParamSchema(BaseModel):
    text: str = Field(alias="#text")
    name: str = Field(alias="@name")


class OfferSchema(BaseModel):
    pk: int = Field(alias="@id")
    available: bool = Field(alias="@available")
    group_id: str = None
    categories_ids: list[int] = Field(alias="categoryId")
    type_prefix: str = Field(default="123", alias="typePrefix")
    params: list[OfferParamSchema] = Field(alias="param")
    barcode: str
    description: str = Field(default="123")
    name: str
    old_price: int = None
    price: int

    @validator("categories_ids", pre=True)
    def validate_categories(cls, value: int):
        return [value]

    @validator("description")
    def validate_description(cls, value):
        return value.replace(":", "\:")

    @validator("params", pre=True)
    def validate_param(cls, value):
        if isinstance(value, dict):
            return [value]
        return value


class CategorySchema(BaseModel):
    pk: int = Field(alias="@id")
    text: str = Field(alias="#text")
    parent_id: int = Field(default=None, alias="parentId")
