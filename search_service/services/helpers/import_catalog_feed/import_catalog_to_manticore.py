import asyncio
import json

from search_service.api.catalog.services.catalog_indexer import CategoriesIndexerService, OffersIndexerService
from search_service.app import app_config
from search_service.app.manticore import ManticoreSearchConnector
from search_service.services.helpers.import_catalog_feed.schemas import CategorySchema, OfferSchema


def parse_catalog(file_name: str) -> tuple[list[dict], list[dict]]:
    with open(file_name, "r") as file:
        data: dict = json.load(file)
        categories: list[dict] = data["yml_catalog"]["shop"]["categories"]["category"]
        offers: list[dict] = data["yml_catalog"]["shop"]["offers"]["offer"]

        return categories, offers


async def import_catalog_to_manticore(file_name: str):
    categories: list[dict]
    offers: list[dict]

    categories, offers = parse_catalog(file_name)

    categories_objs: list[CategorySchema] = [CategorySchema(**item) for item in categories]

    offers_objs: list[OfferSchema] = [OfferSchema(**item) for item in offers]

    manticore_connect = ManticoreSearchConnector(url=app_config.MANTICORE_URL)

    async with manticore_connect.get_connector() as session:
        offers_indexer = OffersIndexerService(session)
        category_indexer = CategoriesIndexerService(session)

    await offers_indexer.replace(offers_objs)
    await category_indexer.replace(categories_objs)


if __name__ == "__main__":
    asyncio.run(import_catalog_to_manticore(input("File path to catalog feed: ")))
