from dataclasses import dataclass


@dataclass
class OfferParamDto:
    pk: int
    text: str
    name: str
    offer_id: int
