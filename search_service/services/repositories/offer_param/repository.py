from sqlalchemy.ext.asyncio import AsyncSession

from search_service.services import SearchService
from search_service.services.integrations.manticore_search.exceptions import ManticoreSearchDBAPIError
from search_service.services.repositories.exceptions import RepositoryOperationError
from search_service.services.repositories.offer_param.dataclasses import OfferParamDto


class OfferParamRepository:
    def __init__(self, manticore_session: AsyncSession, auto_commit: bool = False):
        self._manticore_session: AsyncSession = manticore_session
        self._table_name: str = "offer_param"
        self._manticore_service: SearchService = SearchService(manticore_session)
        self._auto_commit: bool = auto_commit

    async def add(
        self,
        *,
        pk: int = None,
        text: str,
        name: str,
        offer_id: int,
    ) -> OfferParamDto:

        """
        :raise RepositoryOperationError
        """
        params: dict = {
            "id": pk,
            "text": text,
            "name": name,
            "offer_id": offer_id,
        }
        try:
            await self._manticore_service.insert(
                index_name=self._table_name,
                columns=params,
            )

            if self._auto_commit:
                await self._manticore_session.commit()
            return OfferParamDto(pk=params.pop("id"), **params)
        except ManticoreSearchDBAPIError as exc:
            raise RepositoryOperationError(str(exc))

    async def replace(
        self,
        *,
        pk: int,
        text: str,
        name: str,
        offer_id: int,
    ) -> OfferParamDto:

        """
        :raise RepositoryOperationError
        """
        params: dict = {
            "id": pk,
            "text": text,
            "name": name,
            "offer_id": offer_id,
        }
        try:
            await self._manticore_service.replace(
                index_name=self._table_name,
                columns=params,
            )

            if self._auto_commit:
                await self._manticore_session.commit()
            return OfferParamDto(pk=params.pop("id"), **params)
        except ManticoreSearchDBAPIError as exc:
            raise RepositoryOperationError(str(exc))

    async def delete_by_id(self, pk: int) -> None:
        """
        :raise RepositoryOperationError
        """
        try:
            await self._manticore_service.delete_by_equality(
                index_name=self._table_name,
                field="id",
                value=pk,
            )
            if self._auto_commit:
                await self._manticore_session.commit()

        except ManticoreSearchDBAPIError as exc:
            raise RepositoryOperationError(str(exc))

    async def get_by_id(self, pk: int) -> OfferParamDto | None:
        """
        :raise RepositoryOperationError
        """
        try:
            offer_param_raw: dict | None = await self._manticore_service.select_row_by_field(
                index_name=self._table_name,
                field="id",
                value=pk,
            )
            if not offer_param_raw:
                return None

            return OfferParamDto(pk=offer_param_raw.pop("id"), **offer_param_raw)
        except ManticoreSearchDBAPIError as exc:
            raise RepositoryOperationError(str(exc))

    async def delete_by_offer_id(self, offer_id: int):
        """
        :raise RepositoryOperationError
        """
        try:
            await self._manticore_service.delete_by_equality(
                index_name=self._table_name,
                field="offer_id",
                value=offer_id,
            )

            if self._auto_commit:
                await self._manticore_session.commit()
        except ManticoreSearchDBAPIError as exc:
            raise RepositoryOperationError(str(exc))
