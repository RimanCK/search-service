from enum import Enum


class IndexEnum(Enum):
    offer: str = "offer"
    offer_param: str = "offer_param"
    category: str = "category"
