class RepositoryBaseException(Exception):
    def __init__(self, message: str):
        super().__init__(message)


class RepositoryOperationError(RepositoryBaseException):
    pass


class RepositoryFormatDataError(RepositoryBaseException):
    pass
