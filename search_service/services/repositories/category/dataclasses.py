from dataclasses import dataclass


@dataclass
class CategoryDto:
    pk: int
    text: str
    parent_id: int = None
