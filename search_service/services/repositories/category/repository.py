from sqlalchemy.ext.asyncio import AsyncSession

from search_service.services import SearchService
from search_service.services.integrations.manticore_search.exceptions import ManticoreSearchDBAPIError
from search_service.services.repositories.category.dataclasses import CategoryDto
from search_service.services.repositories.exceptions import RepositoryOperationError


class CategoryRepository:
    def __init__(self, manticore_session: AsyncSession, auto_commit: bool = False):
        self._manticore_session: AsyncSession = manticore_session
        self._table_name: str = "category"
        self._manticore_service: SearchService = SearchService(manticore_session)
        self._auto_commit: bool = auto_commit

    async def add(
        self,
        *,
        pk: int,
        text: str,
        parent_id: int = None,
    ) -> CategoryDto:
        """
        :raise RepositoryOperationError
        """
        params: dict = {
            "id": pk,
            "text": text,
            "parent_id": parent_id,
        }
        try:
            await self._manticore_service.insert(
                index_name=self._table_name,
                columns=params,
            )
            if self._auto_commit:
                await self._manticore_session.commit()
            return CategoryDto(pk=params.pop("id"), **params)
        except ManticoreSearchDBAPIError as exc:
            raise RepositoryOperationError(str(exc))

    async def replace(
        self,
        *,
        pk: int,
        text: str,
        parent_id: int = None,
    ) -> CategoryDto:

        """
        :raise RepositoryOperationError
        """
        params: dict = {
            "id": pk,
            "text": text,
            "parent_id": parent_id,
        }
        try:
            await self._manticore_service.replace(
                index_name=self._table_name,
                columns=params,
            )

            if self._auto_commit:
                await self._manticore_session.commit()
            return CategoryDto(pk=params.pop("id"), **params)
        except ManticoreSearchDBAPIError as exc:
            raise RepositoryOperationError(str(exc))

    async def delete_by_id(self, pk: int) -> None:
        """
        :raise RepositoryOperationError
        """
        try:
            await self._manticore_service.delete_by_equality(
                index_name=self._table_name,
                field="id",
                value=pk,
            )

            if self._auto_commit:
                await self._manticore_session.commit()
        except ManticoreSearchDBAPIError as exc:
            raise RepositoryOperationError(str(exc))

    async def get_by_id(self, pk: int) -> CategoryDto | None:
        """
        :raise RepositoryOperationError
        """
        try:
            category_raw: dict | None = await self._manticore_service.select_row_by_field(
                index_name=self._table_name,
                field="id",
                value=pk,
            )
            if not category_raw:
                return None

            return CategoryDto(pk=category_raw.pop("id"), **category_raw)
        except ManticoreSearchDBAPIError as exc:
            raise RepositoryOperationError(str(exc))

    async def get_by_name(self, name: str):
        """
        :raise RepositoryOperationError
        """
        try:
            category_raw: dict | None = await self._manticore_service.select_row_by_field(
                index_name=self._table_name,
                field="text",
                value=name,
            )
            if not category_raw:
                return None

            return CategoryDto(pk=category_raw.pop("id"), **category_raw)
        except ManticoreSearchDBAPIError as exc:
            raise RepositoryOperationError(str(exc))
