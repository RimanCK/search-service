from dataclasses import dataclass


@dataclass
class OfferParamDto:
    text: str
    name: str
    offer_id: int
    pk: int = None


@dataclass
class OfferDto:
    pk: int
    available: bool
    categories_ids: list[int]
    type_prefix: str
    barcode: str
    description: str
    name: str
    price: int
    group_id: str = None


@dataclass
class OfferIndexWithParamsDto:
    pk: int
    available: bool
    categories_ids: list[int]
    type_prefix: str
    barcode: str
    description: str
    name: str
    price: int
    params: list[OfferParamDto] = None
    group_id: str = None
