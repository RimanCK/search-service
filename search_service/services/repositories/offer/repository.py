from sqlalchemy.ext.asyncio import AsyncSession

from search_service.services import SearchService
from search_service.services.integrations.manticore_search.exceptions import ManticoreSearchDBAPIError
from search_service.services.repositories.exceptions import RepositoryOperationError
from search_service.services.repositories.offer.dataclasses import OfferDto, OfferIndexWithParamsDto, OfferParamDto


class OfferRepository:
    def __init__(self, manticore_session: AsyncSession, auto_commit: bool = False):
        self._manticore_session: AsyncSession = manticore_session
        self._table_name: str = "offer"
        self._related_table: str = "offer_param"
        self._manticore_service: SearchService = SearchService(manticore_session)
        self._auto_commit: bool = auto_commit

    async def add(
        self,
        *,
        pk: int,
        available: bool,
        categories_ids: list[int],
        type_prefix: str,
        barcode: str,
        description: str,
        name: str,
        price: int,
        group_id: str = None,
    ) -> OfferDto:
        """
        :raise RepositoryOperationError
        """
        params: dict = {
            "id": pk,
            "available": available,
            "categories_ids": categories_ids,
            "type_prefix": type_prefix,
            "barcode": barcode,
            "description": description,
            "name": name,
            "price": price,
            "group_id": group_id,
        }
        try:
            await self._manticore_service.insert(
                index_name=self._table_name,
                columns=params,
            )

            if self._auto_commit:
                await self._manticore_session.commit()
            return OfferDto(pk=params.pop("id"), **params)
        except ManticoreSearchDBAPIError as exc:
            raise RepositoryOperationError(str(exc))

    async def replace(
        self,
        *,
        pk: int,
        available: bool,
        categories_ids: list[int],
        type_prefix: str,
        barcode: str,
        description: str,
        name: str,
        price: int,
        group_id: str = None,
    ) -> OfferDto:
        """
        :raise RepositoryOperationError
        """
        params: dict = {
            "id": pk,
            "available": available,
            "categories_ids": categories_ids,
            "type_prefix": type_prefix,
            "barcode": barcode,
            "description": description,
            "name": name,
            "price": price,
            "group_id": group_id,
        }
        try:
            await self._manticore_service.replace(
                index_name=self._table_name,
                columns=params,
            )

            if self._auto_commit:
                await self._manticore_session.commit()
            return OfferDto(pk=params.pop("id"), **params)
        except ManticoreSearchDBAPIError as exc:
            raise RepositoryOperationError(str(exc))

    async def delete_by_id(self, pk: int) -> None:
        """
        :raise RepositoryOperationError
        """
        try:
            await self._manticore_service.delete_by_equality(
                index_name=self._table_name,
                field="id",
                value=pk,
            )
            if self._auto_commit:
                await self._manticore_session.commit()
        except ManticoreSearchDBAPIError as exc:
            raise RepositoryOperationError(str(exc))

    async def get_by_id(self, pk: int) -> OfferDto | None:
        """
        :raise RepositoryOperationError
        """
        try:
            offer_raw: dict | None = await self._manticore_service.select_row_by_field(
                index_name=self._table_name,
                field="id",
                value=pk,
            )

            if not offer_raw:
                return None

            return OfferDto(offer_raw.pop("id"), **offer_raw)
        except ManticoreSearchDBAPIError as exc:
            raise RepositoryOperationError(str(exc))

    async def get_by_id_with_params(self, pk: int) -> OfferIndexWithParamsDto | None:
        """
        :raise RepositoryOperationError
        """
        try:
            offer_raw: dict | None = await self._manticore_service.select_row_by_field(
                index_name=self._table_name,
                field="id",
                value=pk,
            )

            if not offer_raw:
                return None

            offer_params_raw: list[dict] = await self._manticore_service.select_rows_by_field(
                index_name=self._related_table,
                field="offer_id",
                value=pk,
            )
            offer_params = [OfferParamDto(**item) for item in offer_params_raw]

            return OfferIndexWithParamsDto(pk=offer_raw.pop("id"), params=offer_params, **offer_raw)
        except ManticoreSearchDBAPIError as exc:
            raise RepositoryOperationError(str(exc))
