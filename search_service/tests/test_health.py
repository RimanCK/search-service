from starlette.testclient import TestClient


def test(client: TestClient):
    response = client.get("/health/")

    assert response.status_code == 200
