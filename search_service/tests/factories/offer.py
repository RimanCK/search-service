import random
from dataclasses import dataclass

import factory
from factory import Sequence


@dataclass
class OfferParamData:
    text: str
    name: str
    offer_id: int


@dataclass
class OfferData:
    pk: int
    available: int
    categories_ids: list[int]
    type_prefix: str
    barcode: str
    description: str
    name: str
    price: int
    group_id: str = None


@dataclass
class OfferDataWithParams:
    pk: int
    available: int
    categories_ids: list[int]
    type_prefix: str
    barcode: str
    description: str
    name: str
    price: int
    group_id: str = None
    params: list[OfferParamData] = None


class OfferParamFactory(factory.Factory):
    class Meta:
        model = OfferParamData

    text: str = Sequence(lambda n: f"Питание для качков {n}")
    name: str = Sequence(lambda n: f"Тип товара {n}")
    offer_id: int = None


class OfferWithParamsFactory(factory.Factory):
    class Meta:
        model = OfferDataWithParams

    pk: int = Sequence(lambda n: n + 1)
    available: int = Sequence(lambda n: n)
    categories_ids: list[int] = Sequence(lambda n: random.choice([[1, 2, 3], [4, 5, 6]]))
    type_prefix: str = Sequence(lambda n: random.choice(["Восстанавливающее средство для мозгов", "Восстанавливающее средство для мозговых паразитов"]))
    barcode: str = Sequence(lambda n: f"{80089719207}{n}")
    description: str = Sequence(lambda n: f"Хорошее средство {n}")
    name: str = Sequence(
        lambda n: random.choice(["Optimum", "UNIVERSAL"])
    )
    price: int = Sequence(lambda n: random.choice([10000, 20000]))
    group_id: str = Sequence(lambda n: str(random.randint(100, 10000)))

    @factory.lazy_attribute
    def params(self):
        return [OfferParamFactory(offer_id=self.pk) for item in range(4)]


class OfferFactory(factory.Factory):
    class Meta:
        model = OfferData

    pk: int = Sequence(lambda n: n + 1)
    available: int = Sequence(lambda n: n)
    categories_ids: list[int] = Sequence(lambda n: random.choice([[1, 2, 3], [4, 5, 6]]))
    type_prefix: str = Sequence(lambda n: random.choice(["Восстанавливающее средство для мозгов", "Восстанавливающее средство для мозговых паразитов"]))
    barcode: str = Sequence(lambda n: f"{80089719207}{n}")
    description: str = Sequence(lambda n: f"Хорошее средство {n}")
    name: str = Sequence(
        lambda n: random.choice(["Optimum", "UNIVERSAL"])
    )
    price: int = Sequence(lambda n: random.choice([10000, 20000]))
    group_id: str = Sequence(lambda n: str(random.randint(100, 10000)))
