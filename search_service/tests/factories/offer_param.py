from dataclasses import dataclass

import factory
from factory import Sequence


@dataclass
class OfferParamData:
    pk: int
    text: str
    name: str
    offer_id: int


class OfferParamFactory(factory.Factory):
    class Meta:
        model = OfferParamData

    pk: int = Sequence(lambda n: n + 1)
    text: str = Sequence(lambda n: f"Шоколад {n}")
    name: str = Sequence(lambda n: f"Вкус питания {n}")
    offer_id: int = Sequence(lambda n: n + 1)
