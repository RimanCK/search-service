from dataclasses import dataclass

import factory
from factory import Sequence


@dataclass
class CategoryData:
    pk: int
    text: str
    parent_id: int = None


class CategoryFactory(factory.Factory):
    class Meta:
        model = CategoryData

    pk: int = Sequence(lambda n: n + 1)
    text: str = Sequence(lambda n: f"Для восстановления мышц{n}")
    parent_id: int = Sequence(lambda n: n + 1)
