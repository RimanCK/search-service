from dataclasses import asdict

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from search_service.services import OfferRepository
from search_service.services.repositories.offer.dataclasses import OfferDto
from search_service.tests.factories.offer import OfferData


class TestOfferRepositoryDeleteUnit:
    @pytest.mark.asyncio
    async def test_delete_offer_successful(self, app, offer_factory, manticore_connector):
        offer_data: OfferData = offer_factory()

        session: AsyncSession
        async with manticore_connector.get_connector() as session:
            offer_repository: OfferRepository = OfferRepository(session, auto_commit=True)

            await offer_repository.add(**asdict(offer_data))

            await offer_repository.delete_by_id(offer_data.pk)

            offer: OfferDto | None = await offer_repository.get_by_id(offer_data.pk)

            assert not bool(offer)
