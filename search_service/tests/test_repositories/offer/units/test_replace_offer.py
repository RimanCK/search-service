from dataclasses import asdict

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from search_service.services import OfferRepository
from search_service.services.repositories import OfferDto
from search_service.tests.factories.offer import OfferData


class TestOfferRepositoryReplaceUnit:
    @pytest.mark.asyncio
    async def test_replace_offer_successful(self, app, offer_factory, manticore_connector):
        offer_data: OfferData = offer_factory()

        session: AsyncSession
        async with manticore_connector.get_connector() as session:
            offer_repository: OfferRepository = OfferRepository(session, auto_commit=True)

            offer: OfferDto = await offer_repository.add(**asdict(offer_data))

            offer_data.description = "123123"

            await offer_repository.replace(**asdict(offer_data))

            replaced_offer: OfferDto = await offer_repository.get_by_id(offer_data.pk)

            assert offer_data.description == replaced_offer.description
            assert replaced_offer.description != offer.description
