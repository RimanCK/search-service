from dataclasses import asdict

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from search_service.services import OfferRepository
from search_service.services.repositories.offer.dataclasses import OfferDto
from search_service.tests.factories.offer import OfferData


class TestOfferRepositoryAddUnit:
    @pytest.mark.asyncio
    async def test_add_offer_successful(self, app, offer_factory, manticore_connector):
        offer_data: OfferData = offer_factory()
        offer_data_dict: dict = asdict(offer_data)

        session: AsyncSession
        async with manticore_connector.get_connector() as session:
            offer_repository: OfferRepository = OfferRepository(session, auto_commit=True)
            await offer_repository.add(**asdict(offer_data))

            offer: OfferDto | None = await offer_repository.get_by_id(offer_data.pk)

            assert offer_data_dict == asdict(offer)
