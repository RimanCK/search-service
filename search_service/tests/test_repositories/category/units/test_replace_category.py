from dataclasses import asdict

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from search_service.services import CategoryRepository
from search_service.services.repositories import CategoryDto
from search_service.tests.factories.category import CategoryData


@pytest.mark.asyncio
async def test_replace_category_successful(app, category_factory, manticore_connector):
    category_data: CategoryData = category_factory()

    session: AsyncSession
    async with manticore_connector.get_connector() as session:
        category_repository: CategoryRepository = CategoryRepository(session, auto_commit=True)

        category: CategoryDto = await category_repository.add(**asdict(category_data))

        category_data.text = "123123"

        await category_repository.replace(**asdict(category_data))

        replaced_category: CategoryDto = await category_repository.get_by_id(category_data.pk)

        assert category_data.text == replaced_category.text
        assert category.text != replaced_category.text
