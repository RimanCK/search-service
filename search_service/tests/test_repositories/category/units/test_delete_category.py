from dataclasses import asdict

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from search_service.services import CategoryRepository
from search_service.tests.factories.category import CategoryData


@pytest.mark.asyncio
async def test_delete_category_successful(app, category_factory, manticore_connector):
    category_data: CategoryData = category_factory()
    category_data_dict: dict = asdict(category_data)

    session: AsyncSession
    async with manticore_connector.get_connector() as session:
        category_repository: CategoryRepository = CategoryRepository(session, auto_commit=True)

        await category_repository.add(**category_data_dict)

        await category_repository.delete_by_id(category_data.pk)

        category: None = await category_repository.get_by_id(category_data.pk)

        assert not bool(category)
