from dataclasses import asdict

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from search_service.services.repositories import OfferParamRepository, OfferParamDto
from search_service.tests.factories.offer_param import OfferParamData


@pytest.mark.asyncio
async def test_delete_offer_param_successful(app, offer_param_factory, manticore_connector):
    offer_param_data: OfferParamData = offer_param_factory()

    session: AsyncSession
    async with manticore_connector.get_connector() as session:
        offer_param_repository: OfferParamRepository = OfferParamRepository(session, auto_commit=True)

        offer_param: OfferParamDto = await offer_param_repository.add(**asdict(offer_param_data))

        offer_param_data.text = "123"

        await offer_param_repository.replace(**asdict(offer_param_data))

        replaced_offer_param: OfferParamDto = await offer_param_repository.get_by_id(pk=offer_param_data.pk)

        assert asdict(replaced_offer_param) == asdict(offer_param_data)
        assert replaced_offer_param.text != offer_param.text
