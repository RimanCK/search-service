from dataclasses import asdict

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from search_service.services.repositories import OfferParamRepository, OfferParamDto
from search_service.tests.factories.offer_param import OfferParamData


@pytest.mark.asyncio
async def test_add_offer_param_successful(app, offer_param_factory, manticore_connector):
    offer_param_data: OfferParamData = offer_param_factory()
    offer_param_data_dict: dict = asdict(offer_param_data)

    session: AsyncSession
    async with manticore_connector.get_connector() as session:
        offer_param_repository: OfferParamRepository = OfferParamRepository(session, auto_commit=True)

        await offer_param_repository.add(**offer_param_data_dict)

        param: OfferParamDto | None = await offer_param_repository.get_by_id(offer_param_data.pk)

        assert offer_param_data_dict == asdict(param)
