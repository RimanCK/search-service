from pathlib import Path
from typing import Generator

import alembic.command
import alembic.config
import pytest
from alembic.config import Config
from fastapi.testclient import TestClient
from pytest_factoryboy import register
from sqlalchemy.exc import DBAPIError
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

from search_service.app import app_config
from search_service.app.fast_api import create_app
from search_service.services.repositories import IndexEnum
from search_service.tests.factories.category import CategoryFactory
from search_service.tests.factories.offer import OfferFactory, OfferWithParamsFactory
from search_service.tests.factories.offer_param import OfferParamFactory

register(OfferWithParamsFactory)
register(OfferFactory)
register(CategoryFactory)
register(OfferParamFactory)


@pytest.fixture
def app():
    app = create_app()
    yield app
    app.container.unwire()


@pytest.fixture
def client(app) -> TestClient:
    return TestClient(app)


@pytest.fixture(scope="function", autouse=True)
def auto_migrate_db() -> Generator | None:
    alembic_config: Config = Config(f"{Path(__file__).parent.parent.parent}/alembic.ini")
    alembic.command.upgrade(alembic_config, "head")
    yield
    alembic.command.downgrade(alembic_config, "head")


@pytest.fixture(scope="function", autouse=True)
async def auto_migrate_manticore():
    engine = create_async_engine(app_config.MANTICORE_URL, echo=True)
    session_factory: sessionmaker = sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)

    session: AsyncSession
    async with session_factory() as session:
        try:
            for index in IndexEnum:
                await session.execute("TRUNCATE TABLE {}".format(index.value))
            await session.commit()
        except DBAPIError:
            await session.close()


@pytest.fixture()
def manticore_connector(app):
    return app.container.manticore_search_connector()
