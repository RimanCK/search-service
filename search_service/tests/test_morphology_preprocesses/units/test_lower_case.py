import pytest

from search_service.services.search.morphology.preproceses.lowecase import PreprocessLowerCase


@pytest.mark.asyncio
def test_search_query_to_lower_case(app):
    preprocess_exclude_stop_words: PreprocessLowerCase = app.container.preprocess_to_lower_case()

    search_query: str = "ПРОТЕИНЫ ДЛЯ ПОХУДЕНИЯ"

    search_query_lower_case: str = preprocess_exclude_stop_words.process(search_query)

    assert search_query_lower_case == "протеины для похудения"
