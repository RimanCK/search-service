import pytest

from search_service.services.search.morphology.preproceses.exclude_stop_words import PreprocessExcludeStopWords


@pytest.mark.asyncio
def test_exclude_stop_words(app):
    preprocess_exclude_stop_words: PreprocessExcludeStopWords = app.container.preprocess_exclude_stop_words()

    search_query: str = "протеины от похудения"

    search_query_first_with_expected_stop_words: str = preprocess_exclude_stop_words.process(search_query)

    assert search_query_first_with_expected_stop_words == "протеины похудения"

    search_query: str = "протеины для похудения"

    search_query_first_with_expected_stop_words: str = preprocess_exclude_stop_words.process(search_query)

    assert search_query_first_with_expected_stop_words == "протеины похудения"
