import pytest

from search_service.services.search.morphology import PreprocessExcludePunctuation


@pytest.mark.asyncio
def test_exclude_punctuation(app):
    preprocess_exclude_punctuation: PreprocessExcludePunctuation = app.container.preprocess_exclude_punctuation()

    search_query: str = "#$%&'()*+,-.протеины для похудения#$%&'()*+,-./"

    search_query_first_with_expected_stop_words: str = preprocess_exclude_punctuation.process(search_query)

    assert search_query_first_with_expected_stop_words == "протеины для похудения"

    search_query: str = "протеи%&ны д%&ля поху%&дения"

    search_query_first_with_expected_stop_words: str = preprocess_exclude_punctuation.process(search_query)

    assert search_query_first_with_expected_stop_words == "протеины для похудения"
