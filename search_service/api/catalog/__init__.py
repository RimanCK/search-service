__all__ = ["offers_router", "categories_router"]

from search_service.api.catalog.views.categories import categories_router
from search_service.api.catalog.views.offers import offers_router
