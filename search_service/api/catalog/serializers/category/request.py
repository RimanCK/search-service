from pydantic import Field

from search_service.api.helpers import BaseSchema


class CategorySchema(BaseSchema):
    pk: int = Field(alias="id")
    text: str
    parent_id: int = None

    class Config:
        schema_extra = {
            "example": {"id": 5, "text": "Спортивное питание", "parentId": 3},
        }


class CategoryBulkCreateRequestSchema(BaseSchema):
    categories: list[CategorySchema]


class CategoryBulkUpdateRequestSchema(BaseSchema):
    categories: list[CategorySchema]


class CategoryBulkDeleteRequestSchema(BaseSchema):
    ids: list[int]

    class Config:
        schema_extra = {
            "example": {"ids": [1, 2, 3, 4, 5]},
        }
