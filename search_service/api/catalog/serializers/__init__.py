__all__ = [
    "CategoryBulkCreateRequestSchema",
    "CategoryBulkDeleteRequestSchema",
    "CategoryBulkUpdateRequestSchema",
    "OfferBulkCreateRequestSchema",
    "OfferBulkUpdateRequestSchema",
    "OfferBulkDeleteRequestSchema",
]

from search_service.api.catalog.serializers.category.request import (
    CategoryBulkCreateRequestSchema,
    CategoryBulkDeleteRequestSchema,
    CategoryBulkUpdateRequestSchema,
)
from search_service.api.catalog.serializers.offers.request import (
    OfferBulkCreateRequestSchema,
    OfferBulkUpdateRequestSchema,
    OfferBulkDeleteRequestSchema,
)
