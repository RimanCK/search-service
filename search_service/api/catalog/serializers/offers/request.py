from pydantic import Field

from search_service.api.helpers import BaseSchema


class OfferParamSchema(BaseSchema):
    text: str
    name: str


class OfferSchema(BaseSchema):
    pk: int = Field(alias="id")
    available: bool
    group_id: str = None
    categories_ids: list[int]
    type_prefix: str
    params: list[OfferParamSchema]
    barcode: str
    description: str
    name: str
    old_price: int = None
    price: int

    class Config:
        schema_extra = {
            "example": {
                "id": 1,
                "available": False,
                "barcode": "3380810453102",
                "categoriesIds": [1, 2, 3],
                "description": "Восстанавливающий мышцы пребиотик",
                "name": "DAS",
                "params": [
                    {"text": "50", "name": "Объем, мл"},
                    {"text": "Спортивное питание", "name": "Тип товара"},
                    {"text": "Франция", "name": "Производство"},
                    {"text": "женщин", "name": "Для"},
                    {"text": "false", "name": "adult"},
                ],
                "price": 123,
                "typePrefix": "Восстанавливающее средство",
                "vendor": "CLARINS",
            },
        }


class OfferBulkCreateRequestSchema(BaseSchema):
    offers: list[OfferSchema]


class OfferBulkUpdateRequestSchema(BaseSchema):
    offers: list[OfferSchema]


class OfferBulkDeleteRequestSchema(BaseSchema):
    ids: list[int]

    class Config:
        schema_extra = {
            "example": {"ids": [1, 2, 3, 4, 5]},
        }
