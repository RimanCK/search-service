from dependency_injector.wiring import Provide, inject
from fastapi import Depends
from fastapi.openapi.models import APIKey
from fastapi.routing import APIRouter
from fastapi_utils.cbv import cbv
from starlette import status

from search_service.api.authentication.helpers import verify_api_key
from search_service.api.catalog.serializers import (
    OfferBulkCreateRequestSchema,
    OfferBulkUpdateRequestSchema,
    OfferBulkDeleteRequestSchema,
)
from search_service.api.catalog.services.catalog_indexer import OffersIndexerService
from search_service.app.containers import Container

offers_router = APIRouter()


@cbv(offers_router)
class OfferView:
    api_key: APIKey = Depends(verify_api_key)

    @offers_router.post("/bulk-create/", tags=["offers"], status_code=status.HTTP_204_NO_CONTENT)
    @inject
    async def bulk_create(
        self,
        request_schema: OfferBulkCreateRequestSchema,
        offers_index_service: OffersIndexerService = Depends(Provide[Container.offers_index_service]),
    ):
        await offers_index_service.create(request_schema.offers)

    @offers_router.put("/bulk-update/", tags=["offers"], status_code=status.HTTP_204_NO_CONTENT)
    @inject
    async def bulk_update(
        self,
        request_schema: OfferBulkUpdateRequestSchema,
        offers_index_service: OffersIndexerService = Depends(Provide[Container.offers_index_service]),
    ):
        await offers_index_service.replace(request_schema.offers)

    @offers_router.post("/bulk-delete/", tags=["offers"], status_code=status.HTTP_204_NO_CONTENT)
    @inject
    async def bulk_delete(
        self,
        request_schema: OfferBulkDeleteRequestSchema,
        offers_index_service: OffersIndexerService = Depends(Provide[Container.offers_index_service]),
    ):
        await offers_index_service.delete(request_schema.ids)
