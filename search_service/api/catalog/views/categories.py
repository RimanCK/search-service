from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, Depends
from fastapi.openapi.models import APIKey
from fastapi_utils.cbv import cbv
from starlette import status

from search_service.api.authentication.helpers import verify_api_key
from search_service.api.catalog.serializers import (
    CategoryBulkCreateRequestSchema,
    CategoryBulkUpdateRequestSchema,
    CategoryBulkDeleteRequestSchema,
)
from search_service.api.catalog.services.catalog_indexer import CategoriesIndexerService
from search_service.app.containers import Container

categories_router = APIRouter()


@cbv(categories_router)
class CategoryView:
    api_key: APIKey = Depends(verify_api_key)

    @categories_router.post("/bulk-create/", tags=["categories"], status_code=status.HTTP_204_NO_CONTENT)
    @inject
    async def bulk_create(
        self,
        request_schema: CategoryBulkCreateRequestSchema,
        category_index_service: CategoriesIndexerService = Depends(Provide[Container.category_index_service]),
    ):
        await category_index_service.create(request_schema.categories)

    @categories_router.put("/bulk-update/", tags=["categories"], status_code=status.HTTP_204_NO_CONTENT)
    @inject
    async def bulk_update(
        self,
        request_schema: CategoryBulkUpdateRequestSchema,
        category_index_service: CategoriesIndexerService = Depends(Provide[Container.category_index_service]),
    ):
        await category_index_service.replace(request_schema.categories)

    @categories_router.post("/bulk-delete/", tags=["categories"], status_code=status.HTTP_204_NO_CONTENT)
    @inject
    async def bulk_delete(
        self,
        request_schema: CategoryBulkDeleteRequestSchema,
        category_index_service: CategoriesIndexerService = Depends(Provide[Container.category_index_service]),
    ):
        await category_index_service.delete(request_schema.ids)
