from abc import ABC, abstractmethod

from pydantic import BaseModel


class IndexerManticoreAbstract(ABC):
    @abstractmethod
    async def create(self, data: list[BaseModel]):
        pass

    @abstractmethod
    async def replace(self, data: list[BaseModel]):
        pass

    @abstractmethod
    async def delete(self, data: list[BaseModel]):
        pass

    async def delete_by_offer_id(self, offer_pk: int):
        raise NotImplemented
