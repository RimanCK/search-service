import logging
from typing import AsyncContextManager

from sqlalchemy.ext.asyncio import AsyncSession
from search_service.api.catalog.serializers.category.request import CategorySchema
from search_service.api.catalog.services.catalog_indexer.exceptions import IndexerCatalogError
from search_service.api.catalog.services.catalog_indexer.interfaces import IndexerManticoreAbstract
from search_service.app import app_config
from search_service.services import CategoryRepository
from search_service.services.repositories.exceptions import RepositoryOperationError


PREFIX_LOGGER: str = "INDEXER CATEGORY"

logger = logging.getLogger(app_config.APP_NAME)


class CategoriesIndexerService(IndexerManticoreAbstract):
    def __init__(self, manticore_session: AsyncContextManager):
        self._manticore_session: AsyncContextManager = manticore_session

    async def create(self, data: list[CategorySchema]) -> None:
        """
        :raise ManticoreSearchDBAPIError
        :raise ValidationError
        """

        try:
            session: AsyncSession
            async with self._manticore_session as session:
                category_repository: CategoryRepository = CategoryRepository(session)
                async with session.begin():
                    category: CategorySchema
                    for category in data:
                        await category_repository.add(**category.dict())
        except RepositoryOperationError as exc:
            raise IndexerCatalogError(str(exc))

        logger.debug(f"{PREFIX_LOGGER}: {len(data)} categories successfully created")

    async def replace(self, data: list[CategorySchema]) -> None:
        """
        :raise ManticoreSearchDBAPIError
        :raise ValidationError
        """
        try:
            session: AsyncSession
            async with self._manticore_session as session:
                category_repository: CategoryRepository = CategoryRepository(session)
                async with session.begin():
                    category: CategorySchema
                    for category in data:
                        await category_repository.replace(**category.dict())
        except RepositoryOperationError as exc:
            raise IndexerCatalogError(str(exc))

        logger.debug(f"{PREFIX_LOGGER}: {len(data)} categories successfully created")

    async def delete(self, data: list[int]) -> None:
        """
        :raise ManticoreSearchDBAPIError
        :raise ValidationError
        """
        try:
            session: AsyncSession
            async with self._manticore_session as session:
                category_repository: CategoryRepository = CategoryRepository(session)
                async with session.begin():
                    pk: int
                    for pk in data:
                        await category_repository.delete_by_id(pk)
        except RepositoryOperationError as exc:
            raise IndexerCatalogError(str(exc))

        logger.debug(f"{PREFIX_LOGGER}: {len(data)} categories successfully deleted")
