import logging
from typing import AsyncContextManager

from sqlalchemy.ext.asyncio import AsyncSession

from search_service.api.catalog.serializers.offers.request import (
    OfferSchema,
)
from search_service.api.catalog.services.catalog_indexer.exceptions import IndexerCatalogError
from search_service.api.catalog.services.catalog_indexer.interfaces import IndexerManticoreAbstract
from search_service.services import OfferRepository
from search_service.services.repositories.exceptions import RepositoryOperationError
from search_service.services.repositories.offer_param.repository import OfferParamRepository


PREFIX_LOGGER: str = "INDEXER OFFERS"


class OffersIndexerService(IndexerManticoreAbstract):
    def __init__(self, manticore_session: AsyncContextManager):
        self._manticore_session: AsyncContextManager = manticore_session

    async def create(self, data: list[OfferSchema]) -> None:
        """
        :raise ManticoreSearchDBAPIError
        :raise ValidationError
        """
        try:
            session: AsyncSession
            async with self._manticore_session as session:
                offer_repository: OfferRepository = OfferRepository(session)

                async with session.begin():
                    offer: OfferSchema
                    for offer in data:
                        offer_dict: dict = offer.dict(exclude_none=True)
                        offer_dict.pop("params")
                        await offer_repository.add(**offer_dict)

                offer_param_repository: OfferParamRepository = OfferParamRepository(session)
                async with session.begin():
                    for offer in data:
                        for param in offer.params:
                            await offer_param_repository.add(**param.dict(), offer_id=offer.pk)
        except RepositoryOperationError as exc:
            raise IndexerCatalogError(str(exc))

        logging.debug(f"{PREFIX_LOGGER}: {len(data)} offers successfully created")

    async def replace(self, data: list[OfferSchema]) -> None:
        """
        :raise ManticoreSearchDBAPIError
        :raise ValidationError
        """
        try:
            async with self._manticore_session as session:
                offer_repository: OfferRepository = OfferRepository(session)

                async with session.begin():
                    offer: OfferSchema
                    for offer in data:
                        offer_dict: dict = offer.dict(exclude_none=True)
                        offer_dict.pop("params")

                        await offer_repository.replace(**offer_dict)

                offer_param_repository: OfferParamRepository = OfferParamRepository(session)
                async with session.begin():
                    for offer in data:
                        await offer_param_repository.delete_by_offer_id(offer_id=offer.pk)

                    for offer in data:
                        for param in offer.params:
                            await offer_param_repository.add(**param.dict(), offer_id=offer.pk)
        except RepositoryOperationError as exc:
            raise IndexerCatalogError(str(exc))

        logging.debug(f"{PREFIX_LOGGER}: {len(data)} offers successfully replaced")

    async def delete(self, data: list[int]) -> None:
        """
        :raise ManticoreSearchDBAPIError
        :raise ValidationError
        """
        try:
            async with self._manticore_session as session:
                offer_repository: OfferRepository = OfferRepository(session)

                async with session.begin():
                    pk: int
                    for pk in data:
                        await offer_repository.delete_by_id(pk)

                offer_param_repository: OfferParamRepository = OfferParamRepository(session)
                async with session.begin():
                    offer_id: int
                    for offer_id in data:
                        await offer_param_repository.delete_by_offer_id(offer_id)
        except RepositoryOperationError as exc:
            raise IndexerCatalogError(str(exc))

        logging.debug(f"{PREFIX_LOGGER}: {len(data)} offers successfully deleted")
