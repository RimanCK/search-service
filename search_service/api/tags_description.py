tags_metadata: list[dict] = [
    {
        "name": "categories",
        "description": "Endpoints for create, update, delete categories in catalog",
    },
    {
        "name": "offers",
        "description": "Endpoints for create, update, delete offers in catalog",
    },
]
