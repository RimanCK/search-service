import bcrypt as bcrypt
from fastapi import Security, HTTPException
from fastapi.security.api_key import APIKeyHeader
from starlette.status import HTTP_403_FORBIDDEN

from search_service.app import app_config

API_KEY = app_config.API_KEY_SERVICE_CATALOG_UPDATE

api_key_header = APIKeyHeader(name=app_config.API_KEY_NAME, description=app_config.API_KEY_DESCRIPTION)


async def verify_api_key(
    api_key_header_: str = Security(api_key_header),
) -> APIKeyHeader | HTTPException:

    if bcrypt.checkpw(API_KEY.encode(), api_key_header_.encode()):
        return api_key_header
    else:
        raise HTTPException(status_code=HTTP_403_FORBIDDEN, detail="Could not validate credentials")
