import bcrypt


def generate_api_key(password: str) -> str:
    password_byte: bytes = password.encode()
    hashed: bytes = bcrypt.hashpw(password_byte, bcrypt.gensalt())

    return hashed.decode("utf-8")


if __name__ == "__main__":
    password: str = input("password: ")
    print(f"{generate_api_key(password)}")
