from pydantic import BaseModel


def to_lower_camel_case(snake_str) -> str:
    first, *others = snake_str.split("_")
    return "".join([first.lower(), *map(str.title, others)])


class BaseSchema(BaseModel):
    class Config:
        alias_generator = to_lower_camel_case
