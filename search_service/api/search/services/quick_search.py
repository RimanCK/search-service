from search_service.api.search.services.base_search import BaseSearch


class CodeSearch(BaseSearch):

    def search(self, search_query: str):
        raise NotImplemented
