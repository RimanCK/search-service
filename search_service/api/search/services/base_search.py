from abc import ABC, abstractmethod


class BaseSearch(ABC):
    @abstractmethod
    async def search(self, search_query: str):
        pass
