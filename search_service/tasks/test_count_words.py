from search_service.app.dramatiq_worker import dramatiq


@dramatiq.actor
def count_words(string: str):
    print(f"There are {len(string)} words.")
