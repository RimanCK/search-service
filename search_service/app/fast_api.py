from fastapi import FastAPI, APIRouter

from sentry_sdk.integrations.asgi import SentryAsgiMiddleware

from search_service.api.catalog import categories_router, offers_router
from search_service.api.tags_description import tags_metadata
from search_service.app import app_config
from search_service.app.containers import Container

from search_service.app.logger import init_sentry_logger, init_logger

service_router = APIRouter()


API_V1: str = "/api/v1"

if app_config.DEBUG:
    init_logger()
else:
    init_sentry_logger()


@service_router.get("/health/", tags=["service"])
async def get_health_status_service():
    return {"status": "ok"}


def create_app() -> FastAPI:
    container = Container()
    container.wire(packages=["search_service.api"])

    app: FastAPI = FastAPI(openapi_tags=tags_metadata, title="Search Service")
    app.container = container

    app.add_middleware(SentryAsgiMiddleware)

    app.include_router(service_router)
    app.include_router(categories_router, prefix=f"{API_V1}/catalog/categories")
    app.include_router(offers_router, prefix=f"{API_V1}/catalog/offers")

    return app
