import sentry_sdk

import logging
from logging import config

from sentry_sdk.integrations.logging import LoggingIntegration
from search_service.app import app_config

sentry_logging = LoggingIntegration(
    level=logging.getLevelName(app_config.LOG_LEVEL),
    event_level=logging.ERROR,
)


LOGGING_CONFIG = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "console": {
            "level": app_config.LOG_LEVEL,
            "class": "logging.StreamHandler",
        },
    },
    "loggers": {
        app_config.APP_NAME: {
            "handlers": ["console"],
            "level": app_config.LOG_LEVEL,
        },
    },
}


def init_sentry_logger():
    sentry_sdk.init(
        app_config.SENTRY_URL,
        debug=False,
        integrations=[sentry_logging],
        traces_sample_rate=1.0,
    )


def init_logger():
    logging.config.dictConfig(LOGGING_CONFIG)
