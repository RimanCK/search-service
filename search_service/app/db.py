from logging import getLogger

from sqlalchemy.exc import DBAPIError
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import declarative_base

from search_service.app import app_config

logger = getLogger(app_config.APP_NAME)

SQLModel = declarative_base()


async def get_db_session(session_factory) -> AsyncSession:
    session: AsyncSession
    async with session_factory() as session:
        try:
            yield session
        except DBAPIError:
            await session.rollback()
            logger.exception("DB session rollback because of exception")
            await session.rollback()
