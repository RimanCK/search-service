import dramatiq
from dramatiq import PickleEncoder
from dramatiq.brokers.rabbitmq import RabbitmqBroker

from search_service.app import app_config

rabbitmq_broker: RabbitmqBroker = RabbitmqBroker(url=app_config.RABBITMQ_URL)
dramatiq.set_broker(rabbitmq_broker)
dramatiq.set_encoder(PickleEncoder())
