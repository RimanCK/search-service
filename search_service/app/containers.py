"""Containers module."""

from dependency_injector import containers, providers
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker

from search_service.api.catalog.services.catalog_indexer import CategoriesIndexerService, OffersIndexerService
from search_service.app.config import app_config
from search_service.app.manticore import ManticoreSearchConnector
from search_service.services.search.helpers.stop_words_dicts import StopWordCache
from search_service.services.search.morphology import PreprocessExcludePunctuation
from search_service.services.search.morphology.preproceses.exclude_stop_words import PreprocessExcludeStopWords
from search_service.services.search.morphology.preproceses.lowecase import PreprocessLowerCase


class Container(containers.DeclarativeContainer):

    manticore_engine = providers.Singleton(create_async_engine, app_config.MANTICORE_URL, echo=False)
    manticore_session_factory = providers.Singleton(
        sessionmaker, manticore_engine, class_=AsyncSession, expire_on_commit=False
    )

    manticore_search_connector = providers.Singleton(ManticoreSearchConnector, url=app_config.MANTICORE_URL)

    offers_index_service = providers.Factory(
        OffersIndexerService,
        manticore_session=manticore_search_connector.provided.get_connector.call(),
    )

    category_index_service = providers.Factory(
        CategoriesIndexerService,
        manticore_session=manticore_search_connector.provided.get_connector.call(),
    )

    stop_word_cache = providers.Singleton(StopWordCache, file_with_stop_words=app_config.STOP_WORDS_FILE_PATH)

    preprocess_exclude_stop_words: PreprocessExcludeStopWords = providers.Singleton(
        PreprocessExcludeStopWords,
        stop_words=stop_word_cache.provided.get_stop_words.call(),
    )

    preprocess_to_lower_case: PreprocessLowerCase = providers.Singleton(PreprocessLowerCase)

    preprocess_exclude_punctuation: PreprocessExcludePunctuation = providers.Singleton(PreprocessExcludePunctuation)
