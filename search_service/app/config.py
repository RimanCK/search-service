import os
from pathlib import Path

from pydantic import BaseSettings


class ApplicationConfig(BaseSettings):
    APP_NAME: str
    DEBUG: bool
    LOG_LEVEL: str = "INFO"
    BASE_DIR: str = "search-service"

    API_KEY_SERVICE_CATALOG_UPDATE: str
    API_KEY_DESCRIPTION: str
    API_KEY_NAME: str

    SENTRY_URL: str

    DB_LOG_LEVEL: str = "ERROR"
    DB_HOST: str
    DB_PORT: int
    DB_NAME: str
    DB_LOGIN: str
    DB_PASSWORD: str
    DB_SCHEMA: str

    RABBITMQ_HOST: str
    RABBITMQ_PORT: int
    RABBITMQ_USER: str
    RABBITMQ_PASSWORD: str
    RABBITMQ_VHOST: str

    MANTICORE_HOST: str
    MANTICORE_PORT: int

    STOP_WORDS_FILE: str

    @property
    def DB_URL(self) -> str:
        return f"postgresql+asyncpg://{self.DB_LOGIN}:{self.DB_PASSWORD}@{self.DB_HOST}:{self.DB_PORT}/{self.DB_NAME}"

    @property
    def RABBITMQ_URL(self) -> str:
        return f"amqp://{self.RABBITMQ_USER}:{self.RABBITMQ_PASSWORD}@{self.RABBITMQ_HOST}:{self.RABBITMQ_PORT}/{self.RABBITMQ_VHOST}"

    @property
    def MANTICORE_URL(self) -> str:
        # Manticore not support auth credentials, they are installed due to requirements mysql client
        return f"mysql+aiomysql://user:password@{self.MANTICORE_HOST}:{self.MANTICORE_PORT}/manticore"

    @property
    def BASE_DIR_PATH(self) -> str:
        return str(Path(os.path.dirname(__file__)).parent.parent)

    @property
    def STOP_WORDS_FILE_PATH(self) -> str:
        return os.path.join(self.BASE_DIR_PATH, self.STOP_WORDS_FILE)


app_config: ApplicationConfig = ApplicationConfig()
