__all__ = [
    "SQLModel",
    "app_config",
]

from search_service.app.config import app_config
from search_service.app.db import SQLModel
