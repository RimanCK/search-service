import contextlib
from logging import getLogger

from sqlalchemy import MetaData
from sqlalchemy.dialects.mysql.base import MySQLDialect
from sqlalchemy.exc import DBAPIError
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker, declarative_base

from search_service.app import app_config

logger = getLogger(app_config.APP_NAME)


def prepare_mysql_dialect() -> None:
    MySQLDialect._get_server_version_info = lambda *args, **kwargs: tuple()
    MySQLDialect._get_default_schema_name = lambda *args, **kwargs: None


prepare_mysql_dialect()


class ManticoreSearchConnector:
    def __init__(self, url: str):
        self._manticore_engine = create_async_engine(url, echo=False)
        self._manticore_session_factory = sessionmaker(
            self._manticore_engine, class_=AsyncSession, expire_on_commit=False
        )

    @contextlib.asynccontextmanager
    async def get_connector(self):
        session: AsyncSession
        async with self._manticore_session_factory() as session:
            try:
                yield session
            except DBAPIError as exc:
                await session.rollback()
                await session.close()
                logger.exception(f"Manticore session rollback because of exception, details: {exc}")
