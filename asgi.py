import uvicorn
from fastapi import FastAPI

from search_service.app.fast_api import create_app

app: FastAPI = create_app()


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=5000)
