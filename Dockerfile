FROM python:3.10

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV POETRY_VERSION '1.1.11'

ARG APP_DIR=/usr/src/app
WORKDIR "$APP_DIR"

COPY ./pyproject.toml .

RUN pip install "poetry==$POETRY_VERSION" && poetry config virtualenvs.create false && poetry install --no-dev

# Add project files
COPY . $APP_DIR/

RUN chmod +x ./entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]