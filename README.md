# Search Service
Service for searching products in the catalog

## Deploy development environment
```shell
pip install --user poetry
poetry init
poetry shell
poetry install
```

## Application envs

### Database
* DB_HOST
* DB_PORT
* DB_NAME 
* DB_LOGIN
* DB_PASSWORD
* DB_SCHEMA

### RabbitMq
* RABBITMQ_HOST
* RABBITMQ_PORT
* RABBITMQ_USER
* RABBITMQ_PASSWORD
* RABBITMQ_VHOST

### Manticore Search
* MANTICORE_HOST
* MANTICORE_PORT

## Database migrations
```shell
alembic upgrade head
```
or
```shell
alembic downgrade head
```

## Pre configuration
```shell
cd search-service
poetry shell
alembic upgrade head
```

## Start service
```shell
export ENVIRONMENT="LOCAL"
cd search-service
bash entrypoint.sh
```
## Start worker
```shell
export ENVIRONMENT="WORKER_MODE"
cd search-service
bash entrypoint.sh
```

## REST API
### GET: /health
### response: 
#### code: 200 OK
#### response body:
```json
{
  "status": "ok"
}
```

## Testing
```shell
cd search-service
poetry shell
pytest
```

### Testing with docker-compose
```shell
cd search-service
sudo docker-compose -f docker-compose-test.yml run search-service
```